This application is run upon the first boot of a new decorum installation.
It installs packages selected at install
It installs any OEM drivers defined at install
it configures the linux runtime environment
It allows the user to create groups & logins
It cleans up the new decorum installation and installs any post install patches
